(async function() {

    let themeKind = $(document.body).attr('data-vscode-theme-kind') === 'vscode-dark'?'dark':'light';

    const themeOberver = new MutationObserver((mutation) => {
        console.log(mutation);
        themeKind = $(document.body).attr('data-vscode-theme-kind') === 'vscode-dark'?'dark':'light';
        updateContent();
    });

    themeOberver.observe(document.body, {
        attributes: true,
        attributeFilter: ['data-vscode-theme-kind']
    });

    joint.shapes.sfc = {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        'Stream': joint.shapes.standard.InscribedImage.extend({
            defaults: joint.util.defaultsDeep({
                type: 'sfc.Stream',
                attrs: {
                    name: {
                        y: 0,
                        refX: "50%",
                        refY: "50%",
                        fontWeight: 'bold',
                        textAnchor: "middle",
                        textVerticalAnchor: "middle",
                    },
                    label: {
                        y: -15
                    },
                    border: {
                        strokeWidth: 0,
                        refRx: "35%",
                        refRy: "35%"
                    },
                    header: {
                        fill: "#333333",
                        fontSize: 15,
                        refX: "50%",
                        refY: "0%",
                        refY2: 10,
                        y: -15,
                        fontWeight: 'bold',
                        textAnchor: "middle",
                        textVerticalAnchor: "top"
                    }
                },
                size: {
                    width: 70,
                    height: 70
                },
                ports: {
                    groups: {
                        'in': {
                            position: {
                                name: 'left',
                                args: {
                                    dx: -10
                                }
                            },
                            attrs: {
                                circle: {
                                    magnet: 'passive'
                                }
                            }
                        },
                        'out': {
                            position: {
                                name: 'right',
                                args: {
                                    dx: 10
                                }
                            },
                            attrs: {
                                circle: {
                                    magnet: true
                                }
                            },
                            label: {
                                position: 'right'
                            }
                        }
                    }
                }
            }, joint.shapes.standard.InscribedImage.prototype.defaults),
            markup: joint.shapes.standard.InscribedImage.prototype.markup.concat([
                {
                    tagName: 'text',
                    selector: 'header'
                },
                {
                    tagName: 'text',
                    selector: 'name'
                }
            ])
        })
    };

    const vscode = acquireVsCodeApi();

    const graph = new joint.dia.Graph;

    let documents = [];
    let docMap = {};

    Object.defineProperty(this, 'contextStack', {
        get: () => {
            let active;
            if(docMap[documents.active]) {
                active = docMap[documents.active];
            } else {
                active = documents.find(docRef => docRef.contextStack[0].context._editorAttrs.documentId === documents.active);
                docMap[documents.active] = active;
            }
            if(!active) {return null;}
            return active.contextStack;
        },
        set: (newStack) => {
            if(docMap[documents.active]) {
                active = docMap[documents.active];
            } else {
                active = documents.find(docRef => docRef.contextStack[0].context._editorAttrs.documentId === documents.active);
                docMap[documents.active] = active;
            }
            if(!active) {return;}
            active.contextStack = newStack;
        }
    });

    Object.defineProperty(this, 'context', {
        get: () => {
            if(!contextStack) {return null;}
            return contextStack[contextStack.length - 1].context;
        },
        set: (context) => {
            contextStack[contextStack.length - 1].context = context;
        }
    });
    
    let updating = false;

    function addContext(current, name) {
        if(current._customTypes) {
            Object.keys(current._customTypes).forEach(key => {
                if(typeof current._customTypes[key] !== 'object' || !current._customTypes[key].constructor) {
                    current._customTypes[key] = {constructor: current._customTypes[key], in:['in'], out: ['pipe'], required: {methods: []}};
                }
            });
        }
        const item = {context: current, name: !name?'Home':name};
        contextStack.push(item);
        updateContent();
    }

    function popContext() {
        contextStack.pop();
        updateContent();
    }

    const postDocument = joint.util.debounce(() => {
        vscode.postMessage({type: 'update', text: JSON.stringify(documents.length === 1?documents[0].contextStack[0].context:documents.map(docRef => docRef.contextStack[0].context))});
        //$('#borrar').html('<pre>'+JSON.stringify(context, null, 2)+'</pre>');
    }, 300);

    function getContextPath() {
        let cpath = '/';
        if(contextStack.length === 1) {
            return cpath;
        }
        for(let i = 1; i < contextStack.length; i++) {
            cpath += contextStack[i].name+'/';
        }
        return cpath;
    }

    var toolsView = new joint.dia.ToolsView({
        name: 'basic-tools',
        tools: [new joint.linkTools.Remove()]
    });

    var editTool = joint.elementTools.Button.extend({
        children: [{
            tagName: 'circle',
            selector: 'button',
            attributes: {
                'r': 7,
                'fill': '#001DFF',
                'cursor': 'pointer'
            }
        }, {
            tagName: 'image',
            selector: 'icon',
            attributes: {
                'xlink:href': actions.edit.dark,
                'x': -5.5,
                'y': -6,
                'width': 11,
                'height': 11,
                'pointer-events': 'none'
            }
        }],
        options: {
            distance: 60,
            offset: 0,
            action: function(evt, view, tool) {
                const sfc = view.model.get('sfc');
                const data = {sfc, clonedSFC: {}, id: view.model.id, name: view.model.id.split('/')[1], error: invalidTypeInstance(sfc)};
                $.extend(true, data.clonedSFC, sfc);
                if(!data.menu) {
                    data.menu = 'data';
                }
                if(!data._untrack) {
                    data._untrack = {name: data.name};
                }
                const typeDef = streamTypes[sfc.type] || getCTypes()[sfc.type];
                if(!typeDef) {
                    data.menu = null;
                    data.error = "No type definition found for "+sfc.type;
                }
                data.events = Array.from(new Set(
                    (Object.keys(sfc.pon||{}))
                    .concat((Object.keys(sfc.ponce||{})))
                    .concat((Object.keys(sfc.on||{})))
                    .concat((Object.keys(sfc.once||{})))
                 ));
                overlayState.active = 'element-editor';
                overlayState['element-editor'] = data;
            }
        }
    });

    const paper = new joint.dia.Paper({
        el: $('#paper'),
        height: $('#paper').parent().height(),
        width: $('#paper').parent().width(),
        gridSize: 10,
        model: graph,
        defaultRouter: {name: 'metro'},
        defaultConnector: {name: 'rounded'},
        defaultLink: new joint.shapes.standard.Link({
            attrs: {
                line: {
                    strokeWidth: '1',
                    targetMarker: {
                        type: 'polygon',
                        points: "0,0 6,-3 6,3",
                        fill: themeKind==='dark'?'#ffffff':'#000000',
                        stroke: themeKind==='dark'?'#ffffff':'#000000',
                        strokeWidth: '1'
                    }
                }
            }
        }),
        drawGrid: 'fixedDot',
        validateConnection: function(cellViewS, magnetS, cellViewT, magnetT, end, linkView) {
        // Prevent linking from input ports.
            if (magnetS && magnetS.getAttribute('port-group') === 'in') {
                return false;
            }
            // Prevent linking from output ports to input ports within one element.
            if (cellViewS === cellViewT) {
                return false;
            }
            // Prevent linking to input ports.
            return ['__resolve__', '__reject__'].includes(cellViewT.model.id) || (magnetT && magnetT.getAttribute('port-group') === 'in');
        },
        snapLinks: { radius: 5 }
    });

    paper.on('element:pointerdblclick', function(element) {
        const name = element.model.id.split('/');
        if(name[0] === 'Goal') {
            let sfc = element.model.get('sfc');
            // eslint-disable-next-line @typescript-eslint/naming-convention
            sfc.build = sfc.build||{'__goal__': []};
            addContext(sfc.build, name[1]);
        }
    });

    paper.on('element:pointerclick', function(element) {
        if(element.model.id === '__edit_context') {
            overlayState['context-editor'] = {clonedContext: {}};
            $.extend(true, overlayState['context-editor'].clonedContext, context);
            return overlayState.active = 'context-editor';
        }
        if(element.model.id === '__reorder') {
            return updateContent(true);
        }
        if(element.model.id === '__back') {
            return popContext();
        }
        if(element.model.id === '__docs') {
            overlayState['documents-editor'] = {clonedDocuments: []};
            $.extend(true, overlayState['documents-editor'].clonedDocuments, documents);
            return overlayState.active = 'documents-editor';
        }
    });

    paper.on('element:mouseenter', function(elementView) {
        if(['__resolve__', '__reject__', '__goal__', '__edit_context', '__reorder', '__back', '__docs'].includes(elementView.model.id)) {
            return;
        }
        elementView.addTools(new joint.dia.ToolsView({
            tools: [
                new joint.elementTools.Remove({
                    useModelGeometry: true,
                    y: '10%',
                    x: '85%',
                }),
                new editTool({
                    useModelGeometry: true,
                    y: '10%',
                    x: '65%'
                })
            ]
        }));
    });
    
    paper.on('element:mouseleave', function(elementView) {
        elementView.removeTools();
    });

    paper.on('link:mouseenter', function(linkView) {
        linkView.addTools(toolsView);
    });
    
    paper.on('link:mouseleave', function(linkView) {
        linkView.removeTools();
    });

    graph.on('change:position', function(element, position) {
        if(['__resolve__', '__reject__', '__goal__'].includes(element.id)) {
            // eslint-disable-next-line @typescript-eslint/naming-convention
            context._editorAttrs = context._editorAttrs || {'__resolve__': {}, '__reject__': {}, '__goal__': {}};
            context._editorAttrs[element.id].pos = position;
        } else {
            const id = element.id.split('/')[1];
            if(!context[id]) {return;}
            if(!context[id]._editorAttrs) {context[id]._editorAttrs = {};}
            context[id]._editorAttrs.pos = position;
        }
        postDocument();
    });

    graph.on('change:target', joint.util.debounce(function(element, target) {
        const source = element.source();
        const links = graph.getLinks()
            .filter(link => {
                const src = link.source();
                const tgt = link.target();
                return src.id === source.id && (source.id === '__goal__' || src.port === source.port) && tgt.id && (['__resolve__', '__reject__'].includes(tgt.id) || tgt.port === 'in');
            })
            .map(link => {
                const tgt = link.target();
                if(['__resolve__', '__reject__'].includes(tgt.id)) {
                    return tgt.id;
                }
                const tgtId = tgt.id.split('/');
                return {
                    type: tgtId[0],
                    name: tgtId[1]
                };
            });
        if(source.id === '__goal__') {
            context[source.id] = links;
        } else {
            const attrs = graph.getCell(source.id).getPort(source.port).attrs;
            if(attrs.kind) {
                if(attrs.kind === 'when') {
                    context[source.id.split('/')[1]].when[attrs.pos].dst = links;
                } else if(attrs.kind === 'chain') {
                    context[source.id.split('/')[1]].chain[attrs.event] = links;
                }
            } else {
                context[source.id.split('/')[1]][source.port]= links;
            }
        }
        postDocument();
    }, 100));

    graph.on('remove', function(cell) {
        if(!updating) {
            if(cell.isLink()) {
                const tgt = cell.target();
                if(!tgt.id) {
                    return;
                }
                const tgtId = tgt.id.split('/');
                const src = cell.source();
                
                if(src.id === '__goal__') {
                    context[src.id] = context[src.id].filter(item => (typeof item === 'string' && item !== tgt.id) || (item.name !== tgtId[1] && item.type !== tgtId[0]));;
                } else {
                    const attrs = graph.getCell(src.id).getPort(src.port).attrs;
                    const srcId = src.id.split('/');
                    if(attrs.kind) {
                        if(attrs.kind === 'when') {
                            context[srcId[1]].when[attrs.pos].dst = context[srcId[1]].when[attrs.pos].dst.filter(item => (typeof item === 'string' && item !== tgt.id) || item.name !== tgtId[1] || item.type !== tgtId[0]);
                        } else if(attrs.kind === 'chain') {
                            context[srcId[1]].chain[attrs.event] = context[srcId[1]].chain[attrs.event].filter(item => (typeof item === 'string' && item !== tgt.id) || item.name !== tgtId[1] || item.type !== tgtId[0]);
                        }
                    } else {
                        context[srcId[1]][src.port] = context[srcId[1]][src.port].filter(item => (typeof item === 'string' && item !== tgt.id) || item.name !== tgtId[1] || item.type !== tgtId[0]);
                    }
                }
            } else {
                const id = cell.id.split('/')[1];
                delete context[id];
            }
            postDocument();
        }
    });

    graph.on('add', function(cell) {
        if(cell instanceof joint.shapes.standard.Link || ['__resolve__', '__reject__', '__goal__', '__edit_context', '__reorder', '__back', '__docs'].includes(cell.id)) {
            return;
        }
        const view = paper.findViewByModel(cell);
        let sfc = cell.get('sfc');
        if(sfc && invalidTypeInstance(sfc)) {
            view.highlight();
        }
    });

    const stencilGraph = new joint.dia.Graph;
    const stencilPaper = new joint.dia.Paper({
        el: $('#stencil'),
        width: 90,
        model: stencilGraph,
        interactive: false
    });

    const getCustomTypeLabel = (key) => {
        const ucMatch = key.match(/(^\w)|[A-Z]/g);
        if(ucMatch.length && ucMatch.length <= 3) {
            return ucMatch.join('').toUpperCase();
        }
        const parts = key.split(/\W/);
        if(parts.length && parts.length <= 3) {
            return ucMatch.join('').toUpperCase();
        }
        return key[0].toUpperCase();
    };

    const getCTypes = (level) => {
        if(!level) {level = 0;}
        if(level >= contextStack.length) {return {};}
        return {...(contextStack[level].context._customTypes||{}), ...getCTypes(++level)};
    };

    function drawStencil() {
        stencilGraph.clear();
        const stKeys = Object.keys(streamTypes);
        stKeys.forEach(async (key, i) => {
            const type = streamTypes[key];
            //var svgFile = await fetch(type.light).then(response => response.text());
            let element = new joint.shapes.sfc.Stream({
                id: key,
                //markup: '<g class="rotatable"><g class="scalable"><image class="body"/></g><text class="label"/><g class="inPorts"/><g class="outPorts"/></g>',
                position: {
                    x: 10,
                    y: (i * 90) + 20
                },
                attrs: {
                    label: {
                        text: key,
                    },
                    image: {
                        xlinkHref: type[themeKind]
                    }
                }
            });
            stencilGraph.addCell(element);
            stencilPaper.fitToContent();
        });

        if(!context) {return;}

        const ess = getCTypes();
        Object.keys(ess).forEach(async (key, i) => {
            //const type = ess[key];
            //var svgFile = await fetch(type.light).then(response => response.text());
            
            let element = new joint.shapes.sfc.Stream({
                id: key,
                //markup: '<g class="rotatable"><g class="scalable"><image class="body"/></g><text class="label"/><g class="inPorts"/><g class="outPorts"/></g>',
                position: {
                    x: 10,
                    y: ((stKeys.length  + i) * 90) + 20
                },
                attrs: {
                    label: {
                        text: key,
                    },
                    name: {
                        text: getCustomTypeLabel(key)
                    },
                    border: {
                        strokeWidth: 1
                    }
                }
            });
            stencilGraph.addCell(element);
            stencilPaper.fitToContent();
        });
    }

    drawStencil();

    stencilPaper.on('cell:pointerdown', function(cellView, e, x, y) {
        $('body').append('<div id="flyPaper" style="position:fixed;z-index:100;opacity:.7;pointer-event:none;"></div>');
        var flyGraph = new joint.dia.Graph,
          flyPaper = new joint.dia.Paper({
            el: $('#flyPaper'),
            model: flyGraph,
            width: 70,
            height: 70,
            interactive: false
          }),
          flyShape = cellView.model.clone(),
          pos = cellView.model.position(),
          offset = {
            x: x - pos.x,
            y: y - pos.y
          };
  
        flyShape.position(0, 0);
        flyGraph.addCell(flyShape);
        $("#flyPaper").offset({
          left: e.pageX - offset.x,
          top: e.pageY - offset.y
        });
        $('body').on('mousemove.fly', function(e) {
          $("#flyPaper").offset({
            left: e.pageX - offset.x,
            top: e.pageY - offset.y
          });
        });
        $('body').on('mouseup.fly', function(e) {
          var x = e.pageX,
            y = e.pageY,
            target = paper.$el.offset();
  
          // Dropped over paper ?
          if (x > target.left && x < target.left + paper.$el.width() && y > target.top && y < target.top + paper.$el.height()) {
            var s = flyShape.clone();
            s.position(x - target.left - offset.x, y - target.top - offset.y);
            graph.addCell(s);
            //triggerAttach(s);
            overlayState.active = 'attach';
            overlayState.attach = s;
          }
          $('body').off('mousemove.fly').off('mouseup.fly');
          flyShape.remove();
          $('#flyPaper').remove();
        });
    });

    //###### STATE MANAGEMENT

    function setState(newState) {
        const state = vscode.getState();
        vscode.setState({...state, ...newState});
    }

    const isProxy = Symbol("isProxy");
    function createObserver(target, onStateChange) {
        target = target||{};
        const resolve=(value) => {
            if(value !== null && value !== undefined && !value[isProxy] && typeof value === 'object' && (!value.graph || !graph.getCell(value.cid))) {return createObserver(value, onStateChange);}
            return value;
        };
        if(Array.isArray(target)) {
            target = target.map(resolve);
        } else {
            for(i in target) {
                target[i] = (i==='_untrack')?target[i]:resolve(target[i]);
            }
        }
        return new Proxy(target, {
            set: (target, key, value) => {
                const fireChange = true;
                if(target.hasOwnProperty(key)) {
                    try {
                        fireChange = (typeof target[key] !== typeof value) || (Array.isArray(target[key]) !== Array.isArray(value)) || (JSON.stringify(target[key]) !== JSON.stringify(value));
                    } catch(e) {}
                }
                target[key] = (key==='_untrack')?value:resolve(value);
                if(fireChange) {onStateChange();}
                return true;
            },
            get: (target, key) => {
                if(key === isProxy) {
                    return true;
                }
                return target[key];
            },
            deleteProperty: (target, key) => {
                delete target[key];
                if(key !== '_untrack') {onStateChange();}
            }
        });
    }

    const overlayEvent = {
        events: [],
        on: function(event, cb) {
            if(!this.events[event]) {this.events[event] = [];}
            this.events[event].push(cb);
        },
        emit: function(event, data) {
            (this.events[event]||[]).forEach(cb => setTimeout(() => cb(data)));
        }
    };

    const onOverlayStateChange = joint.util.debounce(() => {
        setState({overlayState});
        $(".overflow, .overflow .card").removeClass('active');
        if(overlayState.active) {
            $(".overflow, .overflow ."+overlayState.active).addClass('active');
            overlayEvent.emit(overlayState.active, overlayState[overlayState.active]);
        }
    }, 300);

    function postPartial(message) {
        vscode.postMessage({...message, type: 'partial', path: getContextPath()+message.path});
    }

    $('.overflow .cancel').on('click', function() {
        overlayState.active = false;
    });

    //####### ATTACH

    $('.overflow .attach .name').on('keyup', function(evt) {
        if(evt.key === 'Enter' && $(this).val() !== '') {
            $('.overflow .attach .accept').trigger('click');
        }
        if(evt.key === 'Escape') {
            $('.overflow .attach .cancel').trigger('click');
        }
    });

    $('.overflow .attach .accept').on('click', function() {
        let s = overlayState.attach;
        const val = $('.overflow .attach .name').val();
        if(!val) { return; }
        updating = true;
        overlayState.active=false;
        const type = s.attr('label/text');
        let typeObj = streamTypes[type] || getCTypes()[type];
        graph.removeCells(s);
        if(typeObj) {
            s = s.clone().set('id', type+'/'+val);
            s.addPorts(typeObj.in.map(port => {
                return {
                    id: port,
                    group: 'in',
                    attrs: { text: { text: port } }
                };
            }).concat(typeObj.out.map(port => {
                return {
                    id: port,
                    group: 'out',
                    attrs: { text: { text: port } }
                };
            })));
            s.attr('header/text', val);
            s.attr('label/text', type);
            if(s.hasPort('in')) {
                s.portProp('in', 'attrs/.port-body/magnet', 'passive');
            }
            context[val] = {
                type: type,
                _editorAttrs: {
                    pos: s.position()
                }
            };
            s.set('sfc', context[val]);
            graph.addCell(s);
            setState({ documents });
        }
        updating = false;
        if(typeObj) {postDocument();}
    });

    $('.overflow .attach .cancel').on('click', function() {
        graph.removeCells(overlayState.attach);
    });

    overlayEvent.on('attach', (s) => {
        $('.overflow .attach .name').val('').trigger('focus');
    });

    //####### Context Edit

    overlayEvent.on('context-editor', () => {
        $('.overflow .context-editor .libraries_list > *, .overflow .context-editor .streams_list > *').remove();
        const clonedContext = overlayState['context-editor'].clonedContext;
        Object.keys(clonedContext._require||[]).forEach(key => {
            const li = $(`<li><span>${key}</span><button class="edit_library" data-key="${key}">Edit</button><button class="remove_library" data-key="${key}">Remove</button></li>`);
            $('.overflow .context-editor .libraries_list').append(li);
        });
        Object.keys(clonedContext._customTypes||[]).forEach(key => {
            const li = $(`<li><span>${key}</span><button class="edit_stream" data-key="${key}">Edit</button><button class="remove_stream" data-key="${key}">Remove</button></li>`);
            $('.overflow .context-editor .streams_list').append(li);
        });
    });

    $('.overflow .context-editor').on('click', '.remove_library', function() {
        const clonedContext = overlayState['context-editor'].clonedContext;
        const data = $(this).data();
        delete clonedContext._require[data.key];
    });

    $('.overflow .context-editor').on('click', '.remove_stream', function() {
        const clonedContext = overlayState['context-editor'].clonedContext;
        const data = $(this).data();
        const findInstances = (context) => {
            let instances = [];
            for(let i in context) {
                if(i === '__goal__' || i === '_editorAttrs') {continue;}
                if(context[i].type === data.key) {
                    instances.push({context, key: i});
                }
                if(context[i].type === 'Goal') {
                    instances = instances.concat(findInstances(context[i].build));
                }
            }
            return instances;
        };

        const removeLinks = (context, type) => {
            for(let i in context) {
                if(i === '__goal__' || i === '_editorAttrs') {continue;}
                const elem = context[i];
                if(elem.when) {
                    elem.when.forEach(when => {
                        if(!Array.isArray(when.dst)) {
                            when.dst = [when.dst];
                        }
                        when.dst = when.dst.filter(dst => dst.type !== type);
                    });
                }
                if(elem.chain) {
                    for(let j in elem.chain) {
                        if(!Array.isArray(elem.chain[j])) {
                            elem.chain[j] = [elem.chain[j]];
                        }
                        elem.chain[j] = elem.chain[j].filter(dst => dst.type !== type);
                    }
                }
                ['pipe', 'none', 'reject', 'resolve'].forEach(port => {
                    if(elem[port]) {
                        if(!Array.isArray(elem[port])) {
                            elem[port] = [elem[port]];
                        }
                        elem[port] = elem[port].filter(dst => dst.type !== type);
                    }
                });
                if(elem.type === 'Goal') {
                    removeLinks(elem.build, type);
                }
            }
        };
        const instances = findInstances(clonedContext);
        if(!instances.length) {
            delete clonedContext._customTypes[data.key];
        } else {
            const state = {
                title: 'Confirm remove custom type',
                description: `
                <div>Type ${data.key} has the following instances: </div>
                <ul>${instances.map(instance => `<li>${instance.key}</li>`).join('')}</ul>
                <div>Are you sure you want to remove it?</div>
                `,
                to: 'context-editor',
                accept: () => {
                    instances.forEach(instance => delete instance.context[instance.key]);
                    removeLinks(clonedContext, data.key);
                    delete clonedContext._customTypes[data.key];
                }
            };

            overlayState['confirm-form'] = state;
            overlayState.active = 'confirm-form';

        }
    });

    $('.overflow .context-editor').on('click', '.edit_library', function() {
        const clonedContext = overlayState['context-editor'].clonedContext;
        if(!clonedContext._require) {
            clonedContext._require = {};
        }

        const data = $(this).data()||{};
        data._untrack = data.key||'';
        overlayState['library-form'] = data;
        overlayState.active = 'library-form';

    });

    $('.overflow .context-editor').on('click', '.edit_stream', function() {
        const clonedContext = overlayState['context-editor'].clonedContext;
        if(!clonedContext._customTypes) {
            clonedContext._customTypes = {};
        }
        const streamData = $(this).data();
        const defaultType = {constructor: '', in: ['in'], out: ['pipe'], required: {methods: []}};
        const state = {_untrack: {key: streamData.key||''}, key: streamData.key, data: (streamData.key && clonedContext._customTypes[streamData.key] && $.extend(true, {}, clonedContext._customTypes[streamData.key]))||defaultType};
        if(typeof state.data === 'string') {
            state.data = {...defaultType, constructor: state.data};
        }
        overlayState['stream-editor'] = state;
        overlayState.active = 'stream-editor';
    });

    $('.overflow .context-editor .accept').on('click', function() {
        const clonedContext = overlayState['context-editor'].clonedContext;
        context = clonedContext;
        postDocument();
        updateContent();
        overlayState.active = false;
    });

    $('.overflow .library-form .back, .overflow .stream-editor .back').click(()=>{
        overlayState.active = 'context-editor';
    });

    //###### Confirm form 

    overlayEvent.on('confirm-form', () => {
        const state = overlayState['confirm-form'];
        $('.overflow .confirm-form .card-title').html(state.title);
        $('.overflow .confirm-form .description').html(state.description);
    });

    $('.overflow .confirm-form .cancel').click(() => {
        const state = overlayState['confirm-form'];
        overlayState.active = state.to;
    });

    $('.overflow .confirm-form .accept').click(() => {
        const state = overlayState['confirm-form'];
        state.accept();
        overlayState.active = state.to;
    });

    //###### Custom type edior

    overlayEvent.on('stream-editor', () => {
        const state = overlayState['stream-editor'];
        $('.overflow .stream-editor .name').val(state._untrack.key);
        $('.overflow .stream-editor .in_pipe').prop('checked', state.data.in.length);
        $('.overflow .stream-editor .out_pipes').each(function() {
            const data = $(this).data();
            $(this).prop('checked', state.data.out && state.data.out.includes(data.method));
        });
        // $('.overflow .stream-editor .when').prop('checked', ((state.data.form && state.data.form.outPipes)||[]).includes('when'));
        // $('.overflow .stream-editor .chain').prop('checked', ((state.data.form && state.data.form.outPipes)||[]).includes('chain'));
        $('.overflow .stream-editor .required_methods_list').html(((state.data.required && state.data.required.methods)||[]).map((methods, i) => {
            let text = methods;
            if(Array.isArray(methods)) {
                const tmpArray = [...methods];
                const last = tmpArray.pop();
                text = [tmpArray.join(', '), last].join(' or ');
            }
            return `<li>${text} <button class="remove_required_method" data-key="${i}">remove</button></li>`;
        }).join(''));
        $('.overflow .stream-editor .error').html(state.error||'');
    });

    $('.overflow .stream-editor .name').keyup(function() {
        const state = overlayState['stream-editor'];
        state._untrack.key = $(this).val();
    });

    $('.overflow .stream-editor .in_pipe').change(function() {
        const data = overlayState['stream-editor'].data;
        const checked = $(this).prop('checked');
        if(checked) {
            data.in = ['in'];
        } else {
            data.in = [];
        }
    });

    $('.overflow .stream-editor .out_pipes').change(function() {
        const outPipeData = $(this).data();
        const checked = $(this).prop('checked');
        const data = overlayState['stream-editor'].data;
        if(!data.out) {data.out = [];}
        if(checked && !data.out.includes(outPipeData.method)) {
            data.out.push(outPipeData.method);
            /*
            if(!data.required) {data.required = {};}
            if(!data.required.methods) {data.required.methods = [];}
            if(!data.required.methods.includes(outPipeData.method)) {
                data.required.methods.push(outPipeData.method);
            }
            */
        }
        if(!checked && data.out.includes(outPipeData.method)) {
            data.out = data.out.filter(out => out !== outPipeData.method);
            /*
            if(data.required && data.required.methods && data.required.methods.includes(outPipeData.method)) {
                data.required.methods = data.required.methods.filter(method => method !== outPipeData.method);
            }
            */
        }
    });

    $('.overflow .stream-editor .set_required_method').click(()=>{
        let value = $('.overflow .stream-editor .required_method_name').val();
        if(value === '') {return;}
        const data = overlayState['stream-editor'].data;
        if(!data.required) {data.required = {};}
        if(!data.required.methods) {data.required.methods = [];}
        value = value.split(/\W+/);
        if(value.length ===1) {value = value[0];}
        if(!data.required.methods.find(methods => Array.isArray(methods)&&Array.isArray(value)?JSON.stringify([...methods].sort())===JSON.stringify([...value].sort()):methods===value)) {
            data.required.methods.push(value);
        }
        $('.overflow .stream-editor .pipe_name').val('');
    });

    $('.overflow .stream-editor').on('click', '.remove_pipe', function() {
        const data = overlayState['stream-editor'].data;
        const pipe = $(this).data();
        data.on = data.on.filter(item => item !== pipe);
    });

    $('.overflow .stream-editor').on('click', '.remove_required_method', function() {
        const data = overlayState['stream-editor'].data;
        const key = $(this).data();
        data.required.methods.splice(key, 1);
    });

    $('.overflow .stream-editor .set_stream_constructor').click(function() {
        const state = overlayState['stream-editor'];
        
        const data = {};
        data._untrack = state._untrack;
        data.context = state.data;
        data.defineKey = false;
        data.body = state.data.constructor || '';
        if(state.key) {
            data.title = 'Edit custom type';
            data.addText = 'Save';
        } else {
            data.title = 'Add custom type';
            data.addText = 'Add';
        }
        data.description = `
        <div>This is the equivalent of doing</div>
        <div class="code">const name = function constructor(options) {return get.stream.somehow()}</div>
        <div>Function must return a stream.</div>
        `;
        data.editFunctionText = 'Edit constructor body';
        data.note = `Later you can use ${data._untrack.key||'this key'} as a type in stream declaration in your yaml/json file`;
        data.back = 'stream-editor';
        data.path = '_customTypes/'+(state._untrack.key||'untitled')+'/constructor';
        data.arguments = ['options'];
        data.staticArguments = true;
        data.save = () =>{
            data.context.constructor = data.body;
        };
        overlayState['function-form'] = data;
        overlayState.active = 'function-form';        
    });
    
    $('.overflow .stream-editor .add').click(() => {
        const state = overlayState['stream-editor'];
        const clonedContext = overlayState['context-editor'].clonedContext;
        const name = state._untrack.key;
        state.error = '';
        if(name === '') {
            return state.error = 'Custom type name is required';
        }
        if(name !== state.key && clonedContext._customTypes[name]) {
            return state.error = 'Custom type with name '+name+' already exists';
        }
        if(state.data.constructor === '') {
            return state.error = 'Custom type constructor is required';
        }
        if(state.key && name !== state.key) {
            delete clonedContext._customTypes[state.key];
        }

        clonedContext._customTypes[name] = state.data;

        overlayState.active = 'context-editor';
    });

    //#### Library form
    overlayEvent.on('library-form', () => {
        $('.overflow .library-form .key, .overflow .library-form .value').val('');
        $('.overflow .library-form .add').html('Add');
        
        const clonedContext = overlayState['context-editor'].clonedContext;
        const data = overlayState['library-form'];
        if(data && (data._untrack || data.key)) {
            $('.overflow .library-form .key').val(data._untrack||data.key);
        }
        if(data && data.key) {
            $('.overflow .library-form .add').html('Save');
            $('.overflow .library-form .value').val(clonedContext._require[data.key]);
        }
        $('.overflow .library-form .error').html('');
        $('.overflow .library-form .add').prop('disabled', !!data.error);
        if(data.error) {
            $('.overflow .library-form .error').html(data.error);
        }
    });

    $('.overflow .library-form .key').keyup(function() {
        const data = overlayState['library-form'];
        data._untrack = $(this).val();
        const clonedContext = overlayState['context-editor'].clonedContext;
        if(data.key !== data._untrack && clonedContext._require[data._untrack]) {
            data.error = 'Key '+data._untrack+' already exists';
        } else if(data.error) {
            data.error = false;
        }
    });

    $('.overflow .library-form .add').click(()=>{
        const clonedContext = overlayState['context-editor'].clonedContext;
        const data = overlayState['library-form'];
        if(data.error) {return;}

        const value = $('.overflow .library-form .value').val();
        if(!data._untrack) {
            return data.error = 'Key is required';
        }
        if(!value) {
            return data.error = 'Value is required';
        }
        if(data.key && data.key !== data._untrack) {
            delete clonedContext._require[data.key];
        }
        clonedContext._require[data._untrack] = value;
        overlayState.active = 'context-editor';
    });

    //##### function form

    overlayEvent.on('function-form', () => {
        const data = overlayState['function-form'];
        //$('.overflow .function-form .defineKey').style('display', data.defineKey?'inline-block':'none');
        $('.overflow .function-form .key').val(data._untrack.key||'');
        $('.overflow .function-form .preview').html('');
        $('.overflow .function-form .add').html(data.addText);
        $('.overflow .function-form .card-title').html(data.title);
        $('.overflow .function-form .description').html(data.description||'');
        $('.overflow .function-form .note').html(data.note||'');
        $('.overflow .function-form .defineKey').css('display', data.defineKey?'block':'none');
        $('.overflow .function-form .arguments').css('display', data.staticArguments?'none':'block');
        $('.overflow .function-form .arguments_list').html((data.arguments||[]).map(arg => {
            return `<li>${arg} <button class="remove_argument" data-arg="${arg}">remove</button></li>`;
        }).join(''));
        $('.overflow .function-form .display_arguments').html((data.arguments||[]).join(', '));
        $('.overflow .function-form .edit_function').html(data.editFunctionText);
       
        if(data && data.body) {
            let lines = data.body.split("\n");
            if(lines.length > 20) {
                lines = lines.slice(0, 19).join("\n")+"\n...";
            } else {
                lines = lines.join("\n");
            }
            $('.overflow .function-form .preview').html(lines);
        }
        $('.overflow .function-form .error').html('');
        $('.overflow .function-form .add').prop('disabled', !!data.error);
        if(data.error) {
            $('.overflow .function-form .error').html(data.error);
        }
    });

    $('.overflow .function-form .back').click(()=>{
        overlayState.active = overlayState['function-form'].back;
    });

    $('.overflow .function-form .edit_function').click(()=>{
        const data = {...overlayState['function-form'], save: null, context: null};

        postPartial({to: 'function-form', ...data, arguments: data.arguments.join(', ')});
    });

    $('.overflow .function-form .key').keyup(function() {
        const data = overlayState['function-form'];
        if(!data.defineKey) {return;}
        data._untrack.key = $(this).val();
        if(data.key !== data._untrack.key && data.context[data._untrack.key]) {
            data.error = 'Key '+data._untrack.key+' already exists';
        } else if(data.error) {
            data.error = false;
        }
    });

    $('.overflow .function-form .argument').keyup(function(evt) {
        if(evt.key === 'Enter' && $(this).val() !== '') {
            $('.overflow .function-form .add_argument').trigger('click');
        }
    });

    $('.overflow .function-form .add_argument').click(()=>{
        const arg = $('.overflow .function-form .argument').val();
        $('.overflow .function-form .argument').val('');
        if(arg && !overlayState['function-form'].arguments.includes(arg)) {
            overlayState['function-form'].arguments.push(arg);
        }
    });

    $('.overflow .function-form').on('click', '.remove_argument', function() {
        const data = $(this).data();
        overlayState['function-form'].arguments = overlayState['function-form'].arguments.filter(argument => argument !== data.arg);
    });

    $('.overflow .function-form .add').click(()=>{
        const data = overlayState['function-form'];
        if(data.error) {return;}
        if(data.defineKey && !data._untrack.key) {
            return data.error = 'Key is required';
        }
        if(!data.body) {
            return data.error = 'function body is required';
        }
        
        if(data.save) {
            data.save();
        } else {
            if(data.key && data.key !== data._untrack.key) {
                data.context[data._untrack.key] = data.context[data.key];
                delete data.context[data.key];
            }
            data.context[data._untrack.key] = data.body;
        }
        
        overlayState.active = data.back;
    });

    //###### Element Edit

    overlayEvent.on('element-editor', () => {
        const data = overlayState['element-editor'];
        const sfc = data.clonedSFC;
        const typeDef = streamTypes[sfc.type] || getCTypes()[sfc.type];
        const ee = $('.overflow .element-editor');
        if(data.menu) {
            $('button.activator[data-to="piping"]').css('display', (!typeDef.out || (!typeDef.out.includes('when') && ! typeDef.out.includes('chain')))?'none':'inline');
            $('#piping.menu-body .when, #piping.menu-body .whens_list').css('display', (!typeDef.out || !typeDef.out.includes('when'))?'none':'inline');
            $('#piping.menu-body .chain, #piping.menu-body .chains_list').css('display', (!typeDef.out || !typeDef.out.includes('chain'))?'none':'inline');
            ee.find('.menu-body').removeClass('active');
            ee.find('#'+data.menu+'.menu-body').addClass('active');
            ee.find('.card-title').html('Edit '+data.id);
            ee.find('.name').val(data._untrack.name);
            ee.find('.type').html(sfc.type);
            ee.find('.options_list').html(Object.keys(sfc.options||{}).map(option => {
                const op = sfc.options[option];
                return `
                <li>
                    <div id="${option}_view_div" class="option_view_div ${op !== null?'active':''}">
                        ${option} = ${op !== null && op.hasOwnProperty('_type') && op._type === 'method'?`function(${(op.params||[]).join(', ')}) { <button class="edit_option" data-option="${option}">Edit body</button> }`:`'${op}' <button class="edit_option" data-option="${option}">Edit</button>`}<button  class="remove_option" data-option="${option}">Remove</button>
                    </div>
                    <div  id="${option}_edit_div" class="option_edit_div ${op === null?'active':''}">
                        <input type="text" id="${option}_option_key" class="option_key" value="${option}"/> = '<input type="text" id="${option}_option_value" class="option_value" value="${op !== null ? op:''}"/>' <button class="option_done" data-option="${option}">Done</button>
                    </div>
                    <div>
                        <input type="checkbox" class="option_type" id="option_${option}" data-option="${option}" ${op !== null && op.hasOwnProperty('_type') && op._type === 'method'?'checked':''}/><label for="option_${option}">option is a method</label>
                    </div>
                </li>
                `;
            }).join(''));
            ee.find('.remove_constructor').css('display', sfc.hasOwnProperty('constructor')?'inline':'none');
            ee.find('.methods_list').html(Object.keys(sfc.methods||{}).map(method => {
                return `<li>${method} <button class="edit_method" data-key="${method}">Edit</button><button class="remove_method" data-key="${method}">Remove</button></li>`;
            }).join(''));
            
            const getEventList = (event, list) => {
                if(!list) {return [];}
                if(!list[event]) {return [];}
                if(!Array.isArray(list[event])) {return [list[event]];}
                return list[event];
            };
            ee.find('.events_list').html(
                data.events.map(event => {
                    let evtDef =  `
                        <div>
                        <h4>${event}</h4>
                        <div>
                            <button data-key="${event}" data-list="on" class="set_event_listener">Append listener</button>
                            <button data-key="${event}" data-list="pon" class="set_event_listener">Prepend listener</button>
                            <button data-key="${event}" data-list="once" class="set_event_listener">Append one-time listener</button>
                            <button data-key="${event}" data-list="ponce" class="set_event_listener">Prepend one-time listener</button>
                        </div>
                        <ul class="listener_list">
                    `;
                    evtDef += getEventList(event, sfc.pon).map((listener, i) => {
                        return `<li>function prepended(${(listener.params||[]).join(', ')}) { <button data-list="pon" class="set_event_listener" data-key="${event}" data-pos="${i}">edit</button> }</li>`;
                    })
                    .concat(getEventList(event, sfc.ponce).map((listener, i) => {
                        return `<li>function prependedOnce(${(listener.params||[]).join(', ')}) { <button data-list="ponce" class="set_event_listener" data-key="${event}" data-pos="${i}">edit</button> }</li>`;
                    }))
                    .concat(getEventList(event, sfc.on).map((listener, i) => {
                        return `<li>function append(${(listener.params||[]).join(', ')}) { <button data-list="on" class="set_event_listener" data-key="${event}" data-pos="${i}">edit</button> }</li>`;
                    }))
                    .concat(getEventList(event, sfc.once).map((listener, i) => {
                        return `<li>function appendOnce(${(listener.params||[]).join(', ')}) { <button data-list="once" class="set_event_listener" data-key="${event}" data-pos="${i}">edit</button> }</li>`;
                    })).join('');
                    evtDef += '</ul></div>';
                    return evtDef;
                }).join('')
            );
            ee.find('.whens_list').html((sfc.when||[]).map((when, i) => {
                const name = (when._editorAttrs && when._editorAttrs.name) || 'when'+i;
                return `<li>${name}: function(payload) {<button data-key="${name}" data-pos="${i}" class="set_when">edit</button>}</li>`;
            }).join(''));
            ee.find('.chains_list').html(Object.keys(sfc.chain||[]).map(event => {
                return `<li>${event} <button data-key="${event}" class="del_chain">remove</button></li>`;
            }).join(''));
        }
        ee.find('.error').html((data.error&&`<ul>${data.error.map(error => `<li>${error}</li>`)}</ul>`)||'');
    });

    $('.overflow .element-editor .card-menu .activator').click(function() {
        const to = $(this).data('to');
        overlayState['element-editor'].menu = to;
    });

    $('.overflow .element-editor .name').keyup(function() {
        overlayState['element-editor']._untrack.name = $(this).val();
    });

    $('.overflow .element-editor .add_option').click(() => {
        const sfc = overlayState['element-editor'].clonedSFC;
        if(!sfc.options) {
            sfc.options = {};
        }
        sfc.options[''] = null;
    });

    $('.overflow .element-editor').on('click', '.edit_option', function() {
        const clonedSFC = overlayState['element-editor'].clonedSFC;
        const data = $(this).data();
        const op = clonedSFC.options[data.option];
        if(op.hasOwnProperty('_type') && op._type === 'method') {
            data._untrack = {key: data.option};
            data.context = clonedSFC.options;
            data.defineKey = false;
            data.body = op.code||'';
            data.title = 'Edit option';
            data.addText = 'Save';
            data.description = `
            <div>Set the body and parameters of the option</div>
            `;
            data.editFunctionText = 'Edit option body';
            data.back = 'element-editor';
            data.path = (overlayState['element-editor']._untrack.key||'untitled')+'/options/'+data.option;
            data.arguments = op.params;
            data.staticArguments = false;
            data.save = () =>{
                op.params = data.arguments;
                op.code = data.body;
            };
            overlayState['function-form'] = data;
            overlayState.active = 'function-form';
        } else {
            $('#'+data.option+'_edit_div').addClass('active');
            $('#'+data.option+'_view_div').removeClass('active');
        }
    });

    $('.overflow .element-editor').on('click', '.remove_option', function() {
        const clonedSFC = overlayState['element-editor'].clonedSFC;
        const data = $(this).data();
        delete clonedSFC.options[data.option];
    });

    $('.overflow .element-editor').on('click', '.option_done', function() {
        const clonedSFC = overlayState['element-editor'].clonedSFC;
        overlayState['element-editor'].error = '';
        const data = $(this).data();
        const key = $('.overflow .element-editor #'+data.option+'_option_key').val();
        const value = $('.overflow .element-editor #'+data.option+'_option_value').val();
        if(data.option !== key && clonedSFC.options[key]) {
            overlayState['element-editor'].error = "Option "+key+" already exists";
            $('.overflow .element-editor #'+data.option+'option_key').val(data.option);
            return;
        }
        if(data.option !== key) {
            clonedSFC.options[key] = clonedSFC.options[data.option];
            delete clonedSFC.options[data.option];
        }
        clonedSFC.options[key] = value;
        $('#'+data.option+'_edit_div').removeClass('active');
        $('#'+data.option+'_view_div').addClass('active');
    });

    $('.overflow .element-editor').on('change', '.option_type', function() {
        const options = overlayState['element-editor'].clonedSFC.options;
        const data = $(this).data();
        if($(this).prop('checked')) {
            options[data.option] = {code: null, params: [], _type: 'method'};
        } else {
            options[data.option] = null;
        }
    });

    $('.overflow .element-editor .remove_constructor').click(() => {
        const clonedSFC = overlayState['element-editor'].clonedSFC;
        delete clonedSFC.constructor;
    });

    $('.overflow .element-editor .constructor').click(() => {
        const clonedSFC = overlayState['element-editor'].clonedSFC;

        const data = {};
        data._untrack = 'constructor';
        data.context = clonedSFC;
        data.defineKey = false;
        data.body = (clonedSFC.hasOwnProperty('constructor')  && clonedSFC.constructor)|| '';
        data.title = 'Edit constructor';
        data.addText = 'Save';
        data.description = `
        <div>Override default constructor for the stream</div>
        <div>Function must return a stream.</div>
        `;
        data.editFunctionText = 'Edit constructor body';
        data.back = 'element-editor';
        data.path = (overlayState['element-editor']._untrack.key||'untitled')+'/constructor';
        data.arguments = ['options'];
        data.staticArguments = true;
        data.save = () =>{
            data.context.constructor = data.body;
        };
        overlayState['function-form'] = data;
        overlayState.active = 'function-form';
    });

    $('.overflow .element-editor').on('click', '.edit_method', function() {
        const clonedSFC = overlayState['element-editor'].clonedSFC;

        const data = $(this).data()||{};
        data._untrack = {key: data.key||''};
        if(!clonedSFC.methods) {
            clonedSFC.methods = {};
        }
        data.context = clonedSFC.methods;
        data.defineKey = true;
        data.body = (clonedSFC.methods[data.key] && clonedSFC.methods[data.key].code) || '';
        if(data.key) {
            data.title = 'Edit method';
            data.addText = 'Save';
        } else {
            data.title = 'Add method';
            data.addText = 'Add';
        }
        
        data.description = `
        <div>Add a callable method to the stream</div>
        `;
        data.editFunctionText = 'Edit method body';
        data.back = 'element-editor';
        data.path = (overlayState['element-editor']._untrack.key||'untitled')+'/methods/'+(data._untrack.key||'untitled');
        data.arguments = (clonedSFC.methods[data.key] && clonedSFC.methods[data.key].params) || [];
        data.staticArguments = false;
        data.save = () =>{
            if(data._untrack.key !== data.key && data.context[data._untrack.key]) {
                return data.error = 'Method with name '+data._untrack.key+' already exists';
            }
            if(data.key && data._untrack.key !== data.key) {
                delete data.context[data.key];
            }
            data.context[data._untrack.key] = {code: data.body, params: data.arguments};
            overlayState['element-editor'].error = invalidTypeInstance(clonedSFC);
        };
        overlayState['function-form'] = data;
        overlayState.active = 'function-form';
    });

    $('.overflow .element-editor').on('click', '.remove_method', function() {
        const data = $(this).data();
        const clonedSFC = overlayState['element-editor'].clonedSFC;
        delete clonedSFC.methods[data.key];
        overlayState['element-editor'].error = invalidTypeInstance(clonedSFC);
    });

    $('.overflow .element-editor .add_event').click(() => {
        const value = $('.overflow .element-editor .event_name').val();
        if(value === '') {return;}
        if(!overlayState['element-editor'].events.includes(value)) {
            overlayState['element-editor'].events.push(value);
        }
        $('.overflow .element-editor .event_name').val('');
    });

    $('.overflow .element-editor').on('click', '.set_event_listener', function() {
        const evtData = $(this).data();
        const clonedSFC = overlayState['element-editor'].clonedSFC;
        if(!clonedSFC[evtData.list]) {
            clonedSFC[evtData.list] = {};
        }

        const data = {};
        data._untrack = evtData.key;
        data.context = clonedSFC[evtData.list];
        data.defineKey = false;
        data.body = (evtData.pos !== undefined && clonedSFC[evtData.list][evtData.key] && clonedSFC[evtData.list][evtData.key][evtData.pos] && clonedSFC[evtData.list][evtData.key][evtData.pos].code) || '';
        data.title = evtData.pos !== undefined?'Edit listener':'Add listener';
        data.addText = evtData.pos !== undefined?'Save':'Add';
        data.description = `
        <div>Set listener for '${evtData.key}' event</div>
        `;
        data.editFunctionText = 'Edit listener body';
        data.back = 'element-editor';
        data.path = (overlayState['element-editor']._untrack.key||'untitled')+'/event/'+evtData.key;
        data.arguments = (evtData.pos !== undefined && clonedSFC[evtData.list][evtData.key] && clonedSFC[evtData.list][evtData.key][evtData.pos] && clonedSFC[evtData.list][evtData.key][evtData.pos].params) || [];
        data.staticArguments = false;
        data.save = () => {
            if(!data.context[evtData.key]) {data.context[evtData.key] = [];}
            if(!Array.isArray(data.context[evtData.key])) {data.context[evtData.key] = [data.context[evtData.key]];}
            if(evtData.pos !== undefined) {
                data.context[evtData.key][evtData.pos] = {...data.context[evtData.key][evtData.pos], code: data.body, params: data.arguments};
            } else {
                data.context[evtData.key].push({code: data.body, params: data.arguments});
            }
        };
        overlayState['function-form'] = data;
        overlayState.active = 'function-form';
    });

    $('.overflow .element-editor').on('click', '.set_when', function() {
        const evtData = $(this).data();
        const clonedSFC = overlayState['element-editor'].clonedSFC;
        if(!clonedSFC.when) {
            clonedSFC.when = [];
        }

        const data = {};
        data._untrack = {key: evtData.key};
        data.context = clonedSFC.when;
        data.defineKey = true;
        data.body = (evtData.pos !== undefined && clonedSFC.when[evtData.pos] && clonedSFC.when[evtData.pos].cond) || '';
        data.title = evtData.pos !== undefined?'Edit when condition':'Add when condition';
        data.addText = evtData.pos !== undefined?'Save':'Add';
        data.description = `
        <div>Set 'when' conditional piping for the stream</div>
        `;
        data.editFunctionText = 'Edit condition body';
        data.back = 'element-editor';
        data.path = (overlayState['element-editor']._untrack.key||'untitled')+'/when/'+evtData.key;
        data.arguments = ['payload'];
        data.staticArguments = true;
        data.save = () => {
            if(!Array.isArray(clonedSFC.when)) {clonedSFC.when = [clonedSFC.when];}
            if(evtData.pos !== undefined) {
                clonedSFC.when[evtData.pos].cond = data.body;
                clonedSFC.when[evtData.pos]._editorAttrs = {name: data._untrack.key};
            } else {
                clonedSFC.when.push({cond: data.body, _editorAttrs: {name: data._untrack.key}});
            }
        };
        overlayState['function-form'] = data;
        overlayState.active = 'function-form';
    });

    $('.overflow .element-editor .add_chain').click(() => {
        const value = $('.overflow .element-editor .chain_name').val();
        if(value === '') {return;}
        if(!overlayState['element-editor'].clonedSFC.chain) {overlayState['element-editor'].clonedSFC.chain = {};}
        if(!overlayState['element-editor'].clonedSFC.chain[value]) {
            overlayState['element-editor'].clonedSFC.chain[value] = [];
        }
        $('.overflow .element-editor .chain_name').val('');
    });

    $('.overflow .element-editor').on('click', '.del_chain', function() {
        const data = $(this).data();
        delete overlayState['element-editor'].clonedSFC.chain[data.key];
    });

    $('.overflow .element-editor .accept').on('click', function() {
        const state = overlayState['element-editor'];
        const clonedSFC = state.clonedSFC;
        const invalid = invalidTypeInstance(clonedSFC);
        if(invalid) {return state.error = invalid;}
        $.extend(true, context[state.name], clonedSFC);
        if(state._untrack.name !== state.name) {
            context[state._untrack.name] = context[clonedSFC.name];
            delete context[clonedSFC.name];
        }
        postDocument();
        overlayState.active = false;
    });

    // ###### Documents editor
    overlayEvent.on('documents-editor', () => {
        const clonedDocuments = overlayState['documents-editor'].clonedDocuments;
        const active = clonedDocuments.find(docRef => docRef.contextStack[0].context._editorAttrs.documentId === clonedDocuments.active);
        $('.overflow .documents-editor .docs_select').html(
            clonedDocuments.map((doc, i) => {
                return `<option value="${doc.contextStack[0].context._editorAttrs.documentId}" ${clonedDocuments.active===doc.contextStack[0].context._editorAttrs.documentId?'selected':''}>Document ${i}</option>`;
            }).join('')
        );
        $('.overflow .documents-editor .doc_description').val((active.contextStack[0].context._editorAttrs && active.contextStack[0].context._editorAttrs.description)||'');
        $('.overflow .documents-editor .remove_doc').css('display', clonedDocuments.length > 1?'inline':'none');
    });

    $('.overflow .documents-editor .docs_select').change(function() {
        const value = $(this).val();
        overlayState['documents-editor'].clonedDocuments.active = value;
    });

    $('.overflow .documents-editor .doc_description').keyup(function() {
        const value = $(this).val();
        const clonedDocuments = overlayState['documents-editor'].clonedDocuments;
        const active = clonedDocuments.find(docRef => docRef.contextStack[0].context._editorAttrs.documentId === clonedDocuments.active);
        if(!active.contextStack[0].context._editorAttrs) {active.contextStack[0].context._editorAttrs = {};}
        active.contextStack[0].context._editorAttrs.description = value;
    });

    $('.overflow .documents-editor .remove_doc').click(() => {
        const clonedDocuments = overlayState['documents-editor'].clonedDocuments;
        overlayState['documents-editor'].clonedDocuments = clonedDocuments.filter((doc, i) => doc.contextStack[0].context._editorAttrs.documentId !== clonedDocuments.active);
        overlayState['documents-editor'].clonedDocuments.active = clonedDocuments[0].contextStack[0].context._editorAttrs.documentId;
    });

    $('.overflow .documents-editor .add_document').click(() => {
        const documentId = getNonce();
        overlayState['documents-editor'].clonedDocuments.push({contextStack: [{context: { _editorAttrs: {documentId}}}]});
        overlayState['documents-editor'].clonedDocuments.active = documentId;
    });

    $('.overflow .documents-editor .accept').on('click', function() {
        const state = overlayState['documents-editor'];
        const clonedDocuments = state.clonedDocuments;
        const prevActive = documents.active;
        $.extend(true, documents, clonedDocuments);
        if(prevActive !== clonedDocuments.active) {
            changeDocument(clonedDocuments.active);
        }
        postDocument();
        overlayState.active = false;
    });

    function invalidTypeInstance(sfc) {
        const type = streamTypes[sfc.type] || getCTypes()[sfc.type];
        if(!type) {return ['Instance type "'+sfc.type+'" is not valid'];}
        const errors = [];
        ((type.required||{}).methods||[]).forEach(method => {
            if(Array.isArray(method)) {
                let hasOneMethod = false;
                for(let i = 0; i < method.length; i++) {
                    if((sfc.methods||{})[method[i]]) {
                        hasOneMethod = true;
                        break;
                    }
                }
                if(!hasOneMethod) {
                    const tmp = [...method];
                    const last = tmp.pop();
                    errors.push('One of '+[tmp.join(', '), last].join(' or ')+' methods is required');
                }
            } else if(!(sfc.methods||{})[method]) {
                errors.push('Method '+method+' is required');
            }
        });
        if(errors.length) {return errors;}
        return false;
    }

    function getNonce() {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 32; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    function changeDocument(id) {
        const current = documents.find(docRef => docRef.contextStack[0].context._editorAttrs.documentId === id);
        if(!current) {return;}
        documents.active = id;
        updateContent();
    }

    function setDocuments(docs) {
        let active = documents.active;
        documents = docs.map(doc => {
            let contextStack = [{context: doc}];
            const prevDoc = documents.find(docRef => docRef.contextStack[0].context._editorAttrs.documentId === doc._editorAttrs.documentId);
            if(prevDoc) {
                contextStack = prevDoc.contextStack;
            }

            if(contextStack.length > 1) {
                for(let i = 1; i < contextStack.length; i++) {
                    let current = contextStack[i];
                    if(!contextStack[i-1].context[current.name]) {
                        contextStack.slice(0, i-1);
                        break;
                    } 
                    current.context = contextStack[i-1].context[current.name].build;
                }
            }
            return {
                contextStack
            };
        });
        if(!active || !documents.find(docRef => docRef.contextStack[0].context._editorAttrs.documentId === active) ) {
            active = documents[0].contextStack[0].context._editorAttrs.documentId;
        }
        changeDocument(active);
    }
    // Handle messages sent from the extension to the webview
	window.addEventListener('message', event => {
		const message = event.data; // The json data that the extension sent
		switch (message.type) {
			case 'update':
                resize();
                setDocuments(message.conf);

				// Then persist state information.
				// This state is returned in the call to `vscode.getState` below when a webview is reloaded.
				setState({ documents });

                break;
            case 'partial':
                overlayState[message.to].body = message.body;
                break;
		}
	});

    async function updateContent(force) {
        updating = true;
        graph.clear();
        let baseX = 0;
        if(!contextStack) {return;}
        if(contextStack.length > 1) {
            const goBack = new joint.shapes.standard.InscribedImage({
                id: '__back',
                attrs: {
                    image: {
                        xlinkHref: actions.back[themeKind],
                    },
                    border: {
                        strokeWidth: 1,
                    },
                    root: {
                        'style': 'cursor: pointer'
                    }
                },
                position: {
                    x: 10,
                    y: 10
                },
                size: {
                    width: 20,
                    height: 20
                }
            });
            graph.addCells([goBack]);
            baseX = 30;
        } else {
            const docs = new joint.shapes.standard.InscribedImage({
                id: '__docs',
                attrs: {
                    image: {
                        xlinkHref: actions.docs[themeKind],
                    },
                    border: {
                        strokeWidth: 1,
                    },
                    root: {
                        'style': 'cursor: pointer'
                    }
                },
                position: {
                    x: 10,
                    y: 10
                },
                size: {
                    width: 20,
                    height: 20
                }
            });
            graph.addCells([docs]);
            baseX = 30;
        }
        const editContext = new joint.shapes.standard.InscribedImage({
            id: '__edit_context',
            attrs: {
                image: {
                    xlinkHref: actions.edit[themeKind],
                },
                border: {
                    strokeWidth: 1,
                },
                root: {
                    'style': 'cursor: pointer'
                }
            },
            position: {
                x: baseX + 10,
                y: 10
            },
            size: {
                width: 20,
                height: 20
            }
        });
        const reorder = new joint.shapes.standard.InscribedImage({
            id: '__reorder',
            attrs: {
                image: {
                    xlinkHref: actions.reorder[themeKind],
                    'pointer-events': 'none'
                },
                border: {
                    strokeWidth: 1
                },
                root: {
                    'style': 'cursor: pointer'
                }
            },
            position: {
                x: baseX + 40,
                y: 10
            },
            size: {
                width: 20,
                height: 20
            }
        });
        graph.addCells([editContext, reorder]);
        const positions = [[]];
        positions.hash = {};
        for(let i in context) {
            if(i === '_editorAttrs' || i === '_require' || i === '_customTypes') {
                continue;
            }
            if(i === '__goal__') {
                const goal = new joint.shapes.standard.Circle({
                    id: i,
                    attrs: {
                        body: {
                            fill: '#000000',
                            refR: 1,
                            magnet: true
                        },
                        label: {
                            text: 'Goal input',
                            y: 20
                        }
                    },
                    size: {
                        width: 10,
                        height: 10
                    },
                    position: {
                        x: 50,
                        y: 50
                    }
                });
                if(force || !context._editorAttrs || !context._editorAttrs['__goal__'] || !context._editorAttrs['__goal__'].pos) {
                    positions[0].push(goal);
                } else {
                    goal.position(context._editorAttrs['__goal__'].pos.x, context._editorAttrs['__goal__'].pos.y);
                }
                
                const resolve = new joint.shapes.standard.Circle({
                    id: '__resolve__',
                    attrs: {
                        body: {
                            fill: '#000000',
                            refR: 1,
                            magnet: 'passive'
                        },
                        label: {
                            text: 'Resolve',
                            y: 20
                        }
                    },
                    size: {
                        width: 10,
                        height: 10
                    }
                });
                if(force || !context._editorAttrs || !context._editorAttrs['__resolve__'] || !context._editorAttrs['__resolve__'].pos) {
                    while(positions.length <= 3) {
                        positions.push([]);
                    }
                    positions[3].push(resolve);
                } else {
                    resolve.position(context._editorAttrs['__resolve__'].pos.x, context._editorAttrs['__resolve__'].pos.y);
                }
                const reject = new joint.shapes.standard.Circle({
                    id: '__reject__',
                    attrs: {
                        body: {
                            fill: '#000000',
                            refR: 1,
                            magnet: 'passive'
                        },
                        label: {
                            text: 'Reject',
                            y: 20
                        }
                    },
                    size: {
                        width: 10,
                        height: 10
                    }
                });
                if(force || !context._editorAttrs || !context._editorAttrs['__reject__'] || !context._editorAttrs['__reject__'].pos) {
                    while(positions.length <= 3) {
                        positions.push([]);
                    }
                    positions[3].push(reject);
                } else {
                    reject.position(context._editorAttrs['__reject__'].pos.x, context._editorAttrs['__reject__'].pos.y);
                }
                graph.addCells([goal, resolve, reject]);
                continue;
            }
            let typeObj = streamTypes[context[i].type];
            let element;
            if(!typeObj) {
                typeObj = getCTypes()[context[i].type];
                if(!typeObj) { continue; }
                element = new joint.shapes.sfc.Stream({
                    id: context[i].type+'/'+i,
                    attrs: {
                        header: {
                            text: i,
                        },
                        label: {
                            text: context[i].type
                        },
                        name: {
                            text: getCustomTypeLabel(context[i].type)
                        },
                        border: {
                            strokeWidth: 1
                        }
                    }
                });
            } else {
                element = new joint.shapes.sfc.Stream({
                    id: context[i].type+'/'+i,
                    attrs: {
                        header: {
                            text: i,
                        },
                        label: {
                            text: context[i].type
                        },
                        image: {
                            xlinkHref: typeObj[themeKind]
                        }
                    }
                });
            }

            element.set('sfc', context[i]);

            if(context[i].when) {
                if(!Array.isArray(context[i].when)) {
                    context[i].when = [context[i].when];
                }
                context[i].when.forEach((when, i) => {
                    const name = !when._editorAttrs || !when._editorAttrs.name?'when'+i:when._editorAttrs.name;
                    if(!element.hasPort('when/'+name)) {
                        if(!when._editorAttrs) {
                            when._editorAttrs = {};
                        }
                        when._editorAttrs.name = name;
                        element.addPort({group: 'out', id: 'when/'+name, attrs: {kind: 'when', pos: i, text: {text: name}}});
                    }
                });
            }
            if(context[i].pipe) {
                if(!element.hasPort('pipe')) {
                    element.addPort({group: 'out', id: 'pipe', attrs: {text: {text: 'pipe'}}});
                }
            }
            if(context[i].none) {
                if(!element.hasPort('none')) {
                    element.addPort({group: 'out', id: 'none', attrs: {text: {text: 'none'}}});
                }
            }
            if(context[i].chain) {
                Object.keys(context[i].chain).forEach((name, i )=> {
                    if(!element.hasPort('chain/'+name)) {
                        element.addPort({group: 'out', id: 'chain/'+name, attrs: {kind: 'chain', event: name, text: {text: name}}});
                    }
                });
            }
            if(context[i].resolve) {
                if(!element.hasPort('resolve')) {
                    element.addPort({group: 'out', id: 'resolve', attrs: {text: {text: 'resolve'}}});
                }
            }
            if(context[i].reject) {
                if(!element.hasPort('reject')) {
                    element.addPort({group: 'out', id: 'reject', attrs: {text: {text: 'reject'}}});
                }
            }
            element.addPorts(typeObj.in.filter(port => !element.hasPort(port)).map(port => {
                return {
                    id: port,
                    group: 'in',
                    attrs: { text: { text: port } }
                };
            }).concat(typeObj.out.filter(port => !['when', 'chain'].includes(port) && !element.hasPort(port)).map(port => {
                return {
                    id: port,
                    group: 'out',
                    attrs: { text: { text: port } }
                };
            })));

            if(force || !context[i]._editorAttrs || !context[i]._editorAttrs.pos) {
                positions[0].push(element);
            } else {
                element.position(context[i]._editorAttrs.pos.x, context[i]._editorAttrs.pos.y);
            }
            graph.addCell(element);
        }

        var mkLink = function(source) {
            return function(dst) {
                const target = ['__resolve__', '__reject__'].includes(dst)?{id: dst}:{id: dst.type+'/'+dst.name, port: 'in'};
                try {
                    if(graph.getCell(target.id)) {
                        graph.addCell(new joint.shapes.standard.Link({
                            source,
                            target,
                            attrs: {
                                line: {
                                    strokeWidth: '1',
                                    targetMarker: {
                                        type: 'polygon',
                                        points: "0,0 6,-3 6,3",
                                        stroke: themeKind==='dark'?'#ffffff':'#000000',
                                        fill: themeKind==='dark'?'#ffffff':'#000000',
                                        strokeWidth: '1'
                                    }
                                }
                            }
                        }));
                    }
                } catch(e) {
                    console.log(e);
                }
            };
        };

        for(let i in context) {
            if(i === '_editorAttrs' || i === '_require' || i === '_customTypes') {
                continue;
            }
            if(i === '__goal__') {
                if(!Array.isArray(context[i])) {
                    context[i] = [context[i]];
                }
                context[i].forEach(mkLink({id: i}));
                continue;
            }

            if(context[i].when) {
                if(!Array.isArray(context[i].when)) {
                    context[i].when = [context[i].when];
                }
                context[i].when.forEach(when => {
                    if(!when.dst) {when.dst = [];}
                    if(!Array.isArray(when.dst)) {
                        when.dst = [when.dst];
                    }
                    const source = {
                        id: context[i].type+'/'+i,
                        port: 'when/'+when._editorAttrs.name
                    };
                    when.dst.forEach(mkLink(source));
                });
            }
            if(context[i].pipe) {
                if(!Array.isArray(context[i].pipe)) {
                    context[i].pipe = [context[i].pipe];
                }
                const source = {
                    id: context[i].type+'/'+i,
                    port: 'pipe'
                };
                context[i].pipe.forEach(mkLink(source));
            }
            if(context[i].none) {
                if(!Array.isArray(context[i].none)) {
                    context[i].none = [context[i].none];
                }
                const source = {
                    id: context[i].type+'/'+i,
                    port: 'none'
                };
                context[i].none.forEach(mkLink(source));
            }
            if(context[i].chain) {
                Object.keys(context[i].chain).forEach(event =>{
                    if(!Array.isArray(context[i].chain[event])) {
                        context[i].chain[event] = [context[i].chain[event]];
                    }
                    const source = {
                        id: context[i].type+'/'+i,
                        port: 'chain/'+event
                    };
                    context[i].chain[event].forEach(mkLink(source));
                });
            }
            if(context[i].resolve) {
                if(!Array.isArray(context[i].resolve)) {
                    context[i].resolve = [context[i].resolve];
                }
                const source = {
                    id: context[i].type+'/'+i,
                    port: 'resolve'
                };
                context[i].resolve.forEach(mkLink(source));
            }
            if(context[i].reject) {
                if(!Array.isArray(context[i].reject)) {
                    context[i].reject = [context[i].reject];
                }
                const source = {
                    id: context[i].type+'/'+i,
                    port: 'reject'
                };
                context[i].reject.forEach(mkLink(source));
            }
        }

        if(positions[0].length) {
            const sources = graph.getSources();
            sources.forEach(source => {
                graph.bfs(source, function(element, distance) {
                    if(!distance) {return;}
                    let found = false, col, row;
                    for(let i=0; i< positions.length; i++) {
                        let column = positions[i];
                        for(let j = 0; j < column.length; j++) {
                            if(element === column[j]) {
                                found = true;
                                col=i;
                                row=j;
                                break;
                            }
                        }
                        if(found) {
                            break;
                        }
                    }
                    if(distance > col) {
                        positions[col].splice(row, 1);
                        while(distance >= positions.length) {
                            positions.push([]);
                        }
                        positions[distance].push(element);
                    }
                }, {deep: true, outbound: true});
            });
            for(let i=0; i< positions.length; i++) {
                let column = positions[i];
                for(let j = 0; j < column.length; j++) {
                    let element = column[j];
                    element.position((i * 200) + 50, (j * 120) + 40);
                }
            }
        }
        paper.fitToContent({minHeight: $('#paper').height(), minWidth: $('#paper').width()});
        updating = false;
        drawStencil();
        setState({ documents });
    }

    function resize() {
        paper.fitToContent({minHeight: $('#paper').height(), minWidth: $('#paper').width()});
    }

    $('window, #paper').on('resize', resize);
    $('document').on('ready', resize);

    const state = vscode.getState();

    let overlayState = {};
    if(state) {
        if(state.documents) {
            try{
                documents = state.documents;
                changeDocument(documents.active||documents[0].contextStack[0].context._editorAttrs.documentId);
            } catch(e) {
                documents = [];
                console.log(e);
            }
        }
        overlayState = state.overlayState||{};
        console.log('desde state');
        console.log(overlayState);
    }

    overlayState = createObserver(overlayState, onOverlayStateChange);

})();