import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';
import * as  yaml from 'js-yaml';

export class File implements vscode.FileStat {

    type: vscode.FileType;
    ctime: number;
    mtime: number;
    size: number;

    name: string;
	data: Uint8Array;
	
	to: string;
	document: vscode.TextDocument;

    constructor(name: string, data: Uint8Array, to: string, document: vscode.TextDocument) {
        this.type = vscode.FileType.File;
        this.ctime = Date.now();
        this.mtime = Date.now();
        this.size = 0;
		this.name = name;
		this.data = data;
		this.to = to;
		this.document = document;
    }
}

export class SFCEditor implements vscode.CustomTextEditorProvider {

    public static register(context: vscode.ExtensionContext): vscode.Disposable {
		const provider = new SFCEditor(context);
		const providerRegistration = vscode.window.registerCustomEditorProvider(SFCEditor.viewType, provider);
		return providerRegistration;
    }
    constructor(
		private readonly context: vscode.ExtensionContext
    ) { }
    
	private static readonly viewType = 'sfe.activate';
	
	private static uiText:any = null;

	private static partials:any = {};

	public static getPartialFile(uri: vscode.Uri): File {
		const key = uri.toString();
		if(!SFCEditor.partials[key]) {
			throw vscode.FileSystemError.FileNotFound();
		}
		return SFCEditor.partials[key];
	}

	public static setPartialContent(uri: vscode.Uri, data: Uint8Array) {
		if(!SFCEditor.partials[uri.toString()]) {
			throw vscode.FileSystemError.FileNotFound(uri);
		}
		const file = SFCEditor.partials[uri.toString()];
		file.data = data;

	}

    resolveCustomTextEditor(document: vscode.TextDocument, webviewPanel: vscode.WebviewPanel, token: vscode.CancellationToken): void | Thenable<void> {
        webviewPanel.webview.options = {
			enableScripts: true,
		};
		webviewPanel.webview.html = this.getHtmlForWebview(webviewPanel.webview, document.fileName);

		const postMessageQueue:any = [];

		function updateWebview() {
			let conf;
			if(document.getText() === null || document.getText().trim() === '') {
				conf = {};
			} else if(document.fileName.substr(-5) === '.json') {
				try {
					conf = JSON.parse(document.getText());
				} catch(e) {
					return vscode.window.showErrorMessage('Invalid JSON: '+e.message);
				}
			} else if(document.fileName.substr(-5) === '.yaml' || ['.sfc', '.yml'].includes(document.fileName.substr(-4))) {
				try {
					conf = yaml.safeLoadAll(document.getText());
				} catch(e) {
					return vscode.window.showErrorMessage('Invalid YAML: '+e.message);
				}
			} else {
				return vscode.window.showErrorMessage('Unrecognized file fomat. It must be one of .json, .yaml, .yml or .sfc');
			}
			if(!Array.isArray(conf)) {
				conf = [conf];
			}
			conf.forEach(conf => {
				if(!conf._editorAttrs || !conf._editorAttrs.documentId) {
					conf._editorAttrs = conf._editorAttrs || {};
					conf._editorAttrs.documentId = SFCEditor.getNonce();
				}
			});

			webviewPanel.webview.postMessage({
				type: 'update',
				conf: conf,
			});
		}

		const changeViewState = webviewPanel.onDidChangeViewState(e => {
			if(webviewPanel.active) {
				let message;
				while(message = postMessageQueue.shift()) {
					webviewPanel.webview.postMessage(message);
				}
			}
		});

		// Hook up event handlers so that we can synchronize the webview with the text document.
		//
		// The text document acts as our model, so we have to sync change in the document to our
		// editor and sync changes in the editor back to the document.
		// 
		// Remember that a single text document can also be shared between multiple custom
		// editors (this happens for example when you split a custom editor)

		const changeDocumentSubscription = vscode.workspace.onDidChangeTextDocument(e => {
			if (e.document.uri.toString() === document.uri.toString()) {
				updateWebview();
			}
			const file: File = SFCEditor.partials[e.document.uri.toString()];
			if(file && file.document.uri.toString() === document.uri.toString()) {
				const message = {
					type: 'partial',
					to: file.to,
					body: file.data.toString()
				};
				if(webviewPanel.active) {
					webviewPanel.webview.postMessage(message);
				} else {
					postMessageQueue.push(message);
				}
			}
		});

		const saveDocumentSubscription = vscode.workspace.onDidSaveTextDocument(e => {
			const file: File = SFCEditor.partials[e.uri.toString()];
			if(file && file.document.uri.toString() === document.uri.toString()) {
				vscode.window.showTextDocument(e.uri, {preview: true, preserveFocus: false})
				.then(() => {
					return vscode.commands.executeCommand('workbench.action.closeActiveEditor');
				});
			}
		});

		// Make sure we get rid of the listener when our editor is closed.
		webviewPanel.onDidDispose(() => {
			changeDocumentSubscription.dispose();
			changeViewState.dispose();
			saveDocumentSubscription.dispose();
		});

		// Receive message from the webview.
		webviewPanel.webview.onDidReceiveMessage(e => {
			switch(e.type) {
				case 'update':
					try {
						this.updateTextDocument(document, JSON.parse(e.text));
					} catch(error) {
						throw new Error('Could not get document as json. Content is not valid json');
					}
					break;
				case 'partial':
					const key = `sfcPartial://${path.join(document.uri.path, e.path)}.js`;
					const uri = vscode.Uri.parse(key);
					SFCEditor.partials[uri.toString()] = new File(e.description, Buffer.from(e.body||''), e.to, document);
					vscode.workspace.openTextDocument(uri)
					.then(document => {
						vscode.window.showTextDocument(document, vscode.ViewColumn.Beside);
					});
					break;
			}
		});

		updateWebview();
    }

    private static getNonce() {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 32; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    private getHtmlForWebview(webview: vscode.Webview, name: string): string {
		// Local path to script and css for the webview
		const backboneUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'js', 'backbone.js')
		));
		const lodashUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'js', 'lodash.js')
		));
		const jqueryUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'js', 'jquery.js')
		));
		const jointUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'js', 'joint.js')
        ));
        const sfcUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'js', 'sfc.js')
		));
		const styleJointUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'css', 'joint.css')
		));
		const styleMainUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'css', 'sfc.css')
		));

		// IMAGES

		const imgDuplexUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'duplex.svg')
		));

		const imgFlowAllUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_all.svg')
		));

		const imgFlowEachUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_each.svg')
		));

		const imgFlowFirstUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_first.svg')
		));

		const imgFlowHoldUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_hold.svg')
		));

		const imgFlowJoinUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_join.svg')
		));

		const imgFlowOneUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_one.svg')
		));

		const imgFlowWaitUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_wait.svg')
		));

		const imgGoalUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'goal.svg')
		));

		const imgReadableUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'readable.svg')
		));

		const imgRuleUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'rule.svg')
		));

		const imgTransformUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'transform.svg')
		));

		const imgWritableUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'writable.svg')
		));

		const imgBackUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'arrow_back-black-18dp.svg')
		));

		const imgEditUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'edit-black-18dp.svg')
		));

		const imgErrorUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'error_outline-black-18dp.svg')
		));

		const imgReorderUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'reorder.svg')
		));

		const imgDocsUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'description-24px.svg')
		));

		// WHITE IMAGES

		const imgDuplexWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'duplex_white.svg')
		));

		const imgFlowAllWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_all_white.svg')
		));

		const imgFlowEachWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_each_white.svg')
		));

		const imgFlowFirstWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_first_white.svg')
		));

		const imgFlowHoldWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_hold_white.svg')
		));

		const imgFlowJoinWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_join_white.svg')
		));

		const imgFlowOneWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_one_white.svg')
		));

		const imgFlowWaitWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'flow_wait_white.svg')
		));

		const imgGoalWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'goal_white.svg')
		));

		const imgReadableWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'readable_white.svg')
		));

		const imgRuleWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'rule_white.svg')
		));

		const imgTransformWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'transform_white.svg')
		));

		const imgWritableWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'writable_white.svg')
		));

		const imgBackWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'arrow_back-white-18dp.svg')
		));

		const imgEditWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'edit-white-18dp.svg')
		));

		const imgErrorWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'error_outline-white-18dp.svg')
		));

		const imgReorderWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'reorder_white.svg')
		));

		const imgDocsWUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(this.context.extensionPath, 'static', 'images', 'description-white-18dp.svg')
		));
		// Use a nonce to whitelist which scripts can be run
		const nonce = SFCEditor.getNonce();

		const editorForeground = new vscode.ThemeColor('editor.foreground');
		const editorBackground = new vscode.ThemeColor('editor.background');

		return /* html */`
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<!--
				Use a content security policy to only allow loading images from https or from our extension directory,
				and only allow scripts that have a specific nonce.
				-->
				<meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src ${webview.cspSource} data:; style-src ${webview.cspSource} 'unsafe-inline'; connect-src ${webview.cspSource}; script-src 'nonce-${nonce}'; font-src ${webview.cspSource} data:">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link nonce="${nonce}" href="${styleJointUri}" rel="stylesheet" />
				<link nonce="${nonce}" href="${styleMainUri}" rel="stylesheet" />
				<title>${name}</title>
			</head>
			<body>
				<div id="container">
					<div class="wrapper">
						<div id="stencil">
						</div>
					</div>
					<div class="wrapper" style="flex: 1">
						<div id="paper">
						</div>
					</div>
					<div style="height: 100%; overflow: auto; max-width: 25%">
						<div id="borrar">
						</div>
					</div>
				</div>
				<div class="overflow">
					<div class="card-wrapper">
						<div class="card attach">
							<div class="card-title">Set a name for the stream</div>
							<div class="card-body">
								<div>
									<label for="stream_name">Stream name</label>
								</div>
								<div>
									<input id="stream_name" type="text" class="name"/>
								</div>
							</div>
							<div class="card-action">
								<button class="accept">Accept</button>
								<button class="cancel">Cancel</button>
							</div>
						</div>
						<div class="card library-form">
							<div class="card-title">Add library</div>
							<div class="card-body">
								<div class="description">
									<div>This is the equivalent of doing</div>
									<div class="code">const key = require('value')</div>
								</div>
								<div>
									const <input class="key" name="key" placeholder="key"/> = require('<input class="value" name="value" placeholder="value"/>')
								</div>
								<div class="note">key can have the form '{destructured, key, names}'</div>
								<div class="section">
									<ul class="libraries_list">
									</ul>
								</div>
							</div>
							<div class="error"></div>
							<div class="card-action">
								<button class="add">Accept</button>
								<button class="back">Back</button>
							</div>
						</div>
						<div class="card confirm-form">
							<div class="card-title"></div>
							<div class="card-body">
								<div class="description"></div>
							</div>
							<div class="card-action">
								<button class="accept">Accept</button>
								<button class="cancel">Cancel</button>
							</div>
						</div>
						<div class="card function-form">
							<div class="card-title"></div>
							<div class="card-body">
								<div class="description">
								</div>
								<div class="arguments">
									<div><input class="argument" placeholder="argument"/><button class="add_argument">Add</button></div>
									<ul class="arguments_list">
									</ul>
								</div>
								<div class="code code-block">
									<div>
										<span class="defineKey">const <input class="key" name="name" placeholder="name"/> = </span>function (<span class="display_arguments"></span>) {
									</div>
									<div>
										<button class="edit_function"></button>
									</div>
									<pre class="preview">
									</pre>
									<div>
									}
									</div>
								</div>
								<div class="note"></div>
							</div>
							<div class="error"></div>
							<div class="card-action">
								<button class="add">Accept</button>
								<button class="back">Back</button>
							</div>
						</div>
						<div class="card context-editor">
							<div class="card-title">Edit Context</div>
							<div class="card-body">
								<!-- div><select class="documents"></select><button class="add_document">Add Document</button></div-->
								<div>
									<h3>Required libraries <button class="edit_library" data-add="true">Add</button></h3>
									<ul class="libraries_list">
									</ul>
								</div>
								<div>
									<h3>Custom Types <button class="edit_stream" data-add="true">Add</button></h3>
									<ul class="streams_list">
									</ul>
								</div>
							</div>
							<div class="card-action">
								<button class="accept">Accept</button>
								<button class="cancel">Cancel</button>
							</div>
						</div>
						<div class="card stream-editor">
							<div class="card-title">Edit custom type</div>
							<div class="card-body">
								<div>
									<label for="stream_name">Custom type name</label>
									<input id="stream_name" class="name"/>
								</div>
								<button class="set_stream_constructor">Set constructor</button>
								<div>
									<h4>In piping</h4>
									<input type="checkbox" class="in_pipe" id="in_check"/><label for="pipe_check">Can be piped to (Isn't instance of Readable pipe)</label>
								</div>
								<div>
									<h4>Out piping</h4>
									<div class="note">If checked, an instance of this type must implement the method, with the following signature: function([dstPipes]) { ... }</div>
									<ul class="pipes_list">
										<li>
											<input type="checkbox" class="out_pipes" data-method="pipe" id="pipe_check"/><label for="pipe_check">pipe</label>
										</li>
										<li>
											<input type="checkbox" class="out_pipes" data-method="none" id="none_check"/><label for="none_check">none</label>
										</li>
										<li>
											<input type="checkbox" class="out_pipes" data-method="resolve" id="resolve_check"/><label for="resolve_check">resolve</label>
										</li>
										<li>
											<input type="checkbox" class="out_pipes" data-method="reject" id="reject_check"/><label for="reject_check">reject</label>
										</li>
									</ul>
								</div>
								<div>
									<h4>Configurable piping</h4>
									<ul>
										<li>
											<input type="checkbox" class="out_pipes" data-method="when" id="when_check"/><label for="when_check">"When" condition</label>
											<div class="note">If checked, an instance of this type must implement the method, with the following signature: function when(function, [dstPipes]) { ... }</div>
										</li>
										<li>
											<input type="checkbox" class="out_pipes" data-method="chain" id="chain_check"/><label for="when_check">Chainable events</label>
											<div class="note">If checked, an instance of this type must implement the method, with the following signature: function chain(string, [dstPipes]) { ... }</div>
										</li>
									</ul>
								</div>
								<div>
									<h4>Required methods</h4>
									<div><input class="required_method_name"/> <button class="set_required_method">Add</button></div>
									<div class="note">To declare that one of many methods is required, declare them separated by coma or space</div>
									<ul class="required_methods_list">
									</ul>
								</div>
							</div>
							<div class="error"></div>
							<div class="card-action">
								<button class="add">Accept</button>
								<button class="back">Back</button>
							</div>
						</div>
						<div class="card element-editor">
							<div class="card-title"></div>
							<div class="card-menu">
								<button class="activator" data-to="data">Data</button>
								<button class="activator" data-to="methods">Methods</button>
								<button class="activator" data-to="events">Events</button>
								<button class="activator" data-to="piping">Piping</button>
							</div>
							<div class="card-body">
								<div id="data" class="menu-body">
									<h3>Data</h3>
									<span class="type"></span> / <input type="text" class="name" placeholder="name"/>
									<h3>Options</h3>
									<div>
										<button class="add_option">Add option</button>
									</div>
									<ul class="options_list">
									</ul>
								</div>
								<div id="methods" class="menu-body">
									<h3>Constructor</h3>
									<div class="note">Override type constructor</div>
									<div>
										<button class="constructor">Set constructor</button> <button class="remove_constructor">remove constructor</button>
									</div>
									<h3>Methods</h3>
									<button class="edit_method">Add method</button>
									<ul class="methods_list">
									</ul>
								</div>
								<div id="events" class="menu-body">
									<h3>Events</h3>
									<div>
										<input type="text" class="event_name"/><button class="add_event">Add</button>
									</div>
									<div class="events_list">
									</div>
								</div>
								<div id="piping" class="menu-body">
									<h3>Special piping</h3>
									<div class="when">
										<button class="set_when">Add when condition</button>
									</div>
									<ul class="whens_list">
									</ul>
									<div class="chain">
										<input type="text" class="chain_name"/><button class="add_chain">Add chain</button>
									</div>
									<ul class="chains_list">
									</ul>
								</div>
							</div>
							<div class="error"></div>
							<div class="card-action">
								<button class="accept">Accept</button>
								<button class="cancel">Cancel</button>
							</div>
						</div>
						<div class="card documents-editor">
							<div class="card-title">Edit documents</div>
							<div class="card-body">
								<div>
									<label for="docs_select">Select current document</label>
									<select id="docs_select" class="docs_select"></select>
									<button class="remove_doc">Remove</button>
								</div>
								<textarea class="doc_description"></textarea>
								<div>
									<button class="add_document">Add document</button>
								</div>
							</div>
							<div class="card-action">
								<button class="accept">Accept</button>
								<button class="cancel">Cancel</button>
							</div>
						</div>
					</div>
				</div>
				<script nonce="${nonce}">
					var editorForeground = "${editorForeground}";
					var editorBackground = "${editorBackground}";
					var actions = {
						back: {
							dark: "${imgBackWUri}",
							light: "${imgBackUri}"
						},
						edit: {
							dark: "${imgEditWUri}",
							light: "${imgEditUri}"
						},
						reorder: {
							dark: "${imgReorderWUri}",
							light: "${imgReorderUri}"
						},
						docs: {
							dark: "${imgDocsWUri}",
							light: "${imgDocsUri}"
						}
					};
					notifications = {
						error: {
							dark: "${imgErrorWUri}",
							light: "${imgErrorUri}"
						}
					};
					var streamTypes = {
						Duplex: {
							dark: "${imgDuplexWUri}",
							light: "${imgDuplexUri}",
							in: ['in'],
							out: ['pipe'],
							form: {
								outPipes: [],
								mandatory: {
									methods: [['write', 'writev'], 'read']
								}
							}
						},
						FlowAll: {
							dark: "${imgFlowAllWUri}",
							light: "${imgFlowAllUri}",
							in: ['in'],
							out: ['pipe', 'none', 'when'],
							required: {methods: []}
						},
						FlowEach: {
							dark: "${imgFlowEachWUri}",
							light: "${imgFlowEachUri}",
							in: ['in'],
							out: ['pipe', 'none'],
							required: {methods: []}
						},
						FlowFirst: {
							dark: "${imgFlowFirstWUri}",
							light: "${imgFlowFirstUri}",
							in: ['in'],
							out: ['pipe'],
							required: {methods: ['identify']}
						},
						FlowHold: {
							dark: "${imgFlowHoldWUri}",
							light: "${imgFlowHoldUri}",
							in: ['in'],
							out: ['pipe'],
							required: {methods: []}
						},
						FlowJoin: {
							dark: "${imgFlowJoinWUri}",
							light: "${imgFlowJoinUri}",
							in: ['in'],
							out: ['pipe'],
							required: {methods: []}
						},
						FlowOne: {
							dark: "${imgFlowOneWUri}",
							light: "${imgFlowOneUri}",
							in: ['in'],
							out: ['pipe', 'none', 'when'],
							required: {methods: []}
						},
						FlowWait: {
							dark: "${imgFlowWaitWUri}",
							light: "${imgFlowWaitUri}",
							in: ['in'],
							out: ['pipe'],
							required: {methods: []}
						},
						Goal: {
							dark: "${imgGoalWUri}",
							light: "${imgGoalUri}",
							in: ['in'],
							out: ['resolve', 'reject'],
							required: {methods: []}
						},
						Readable: {
							dark: "${imgReadableWUri}",
							light: "${imgReadableUri}",
							in: [],
							out: ['pipe'],
							required: {methods: ['read']}
						},
						Rule: {
							dark: "${imgRuleWUri}",
							light: "${imgRuleUri}",
							in: ['in'],
							out: ['resolve', 'reject', 'chain'],
							required: {methods: ['rule']}
						},
						Transform: {
							dark: "${imgTransformWUri}",
							light: "${imgTransformUri}",
							in: ['in'],
							out: ['pipe'],
							required: {methods: ['transform']}
						},
						Writable: {
							dark: "${imgWritableWUri}",
							light: "${imgWritableUri}",
							in: ['in'],
							out: [],
							required: {methods: [['write', 'writev']]}
						}
					};
				</script>
				<script nonce="${nonce}" src="${jqueryUri}"></script>
				<script nonce="${nonce}" src="${lodashUri}"></script>
				<script nonce="${nonce}" src="${backboneUri}"></script>
				<script nonce="${nonce}" src="${jointUri}"></script>
                <script nonce="${nonce}" src="${sfcUri}"></script>
			</body>
			</html>`;
    }
    /**
	 * Write out the json to a given document.
	 */
	private updateTextDocument(document: vscode.TextDocument, json: object) {

		SFCEditor.uiText = (document.fileName.substr(-5) === '.json') ? JSON.stringify(json, null, 2) : (Array.isArray(json)?json:[json]).map(doc => yaml.safeDump(doc)).join('---\n');
		if(document.getText() === SFCEditor.uiText) { return; }
		const edit = new vscode.WorkspaceEdit();

		// Just replace the entire document every time for this example extension.
		// A more complete extension should compute minimal edits instead.
		edit.replace(
			document.uri,
			new vscode.Range(0, 0, document.lineCount, 0),
			SFCEditor.uiText
		);

		return vscode.workspace.applyEdit(edit);
	}
}