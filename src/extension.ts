// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { PartialCodeProvider } from './partialCodeProvider';
import { SFCEditor } from './sfcEditor';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	context.subscriptions.push(SFCEditor.register(context), PartialCodeProvider.register());
	
}

// this method is called when your extension is deactivated
export function deactivate() {}
