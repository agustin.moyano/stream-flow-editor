# Stream Flow Editor

[Stream Flow Editor](https://gitlab.com/agustin.moyano/stream-flow-editor) is a vscode plugin that leverages the visual editor for [Stream Flow Control](https://gitlab.com/agustin.moyano/sfc) (sfc), a very powerful way to orchestrate node streams.

![Simple transformation](doc/images/screenshot1.png)

## Features

### Out of the box streams

Besides NodeJS stream classes, (Duplex <img src="static/images/duplex.svg" width="20" style="vertical-align: middle; width: 20px"/>, Readable <img src="static/images/readable.svg" width="20" style="vertical-align: middle; width: 20px"/>, Transform <img src="static/images/transform.svg" width="20" style="vertical-align: middle; width: 20px"/> and Writable <img src="static/images/writable.svg" width="20" style="vertical-align: middle; width: 20px"/>) you can orchestrate [sfc](https://gitlab.com/agustin.moyano/sfc#api)'s own streams implementation:

*  __[FlowAll](https://gitlab.com/agustin.moyano/sfc#FlowAll)__ <img src="static/images/flow_all.svg" width="20" style="vertical-align: middle; width: 20px"/>: Check all piped streams, and flow data to all which matches criteria.
  ![FlowAll](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/FlowAll_loop.gif)

*  __[FlowEach](https://gitlab.com/agustin.moyano/sfc#FlowEach)__ <img src="static/images/flow_each.svg" width="20" style="vertical-align: middle; width: 20px"/>: Stream every element of an array individually.
  ![FlowEach](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/FlowEach_loop.gif)

*  __[FlowFirst](https://gitlab.com/agustin.moyano/sfc#FlowFirst)__ <img src="static/images/flow_first.svg" width="20" style="vertical-align: middle; width: 20px"/>: Flow the first message from multiple source streams and discard the rest.
  ![FlowFirst](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/FlowFirst_loop.gif)

*  __[FlowHold](https://gitlab.com/agustin.moyano/sfc#FlowHold)__ <img src="static/images/flow_hold.svg" width="20" style="vertical-align: middle; width: 20px"/>: Hold flow from multiple source streams until a condition is met. If no method is overwitten, then holds flow until end is signaled on all sources.
  ![FlowHold](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/FlowHold_loop.gif)

* __[FlowJoin](https://gitlab.com/agustin.moyano/sfc#FlowJoin)__ <img src="static/images/flow_join.svg" width="20" style="vertical-align: middle; width: 20px"/>: Join messages from multiple sources into a single messages. If no method is overwitten, then releases a message when there is exactly one message from each source.
  ![FlowJoin](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/FlowJoin_loop.gif)

* __[FlowOne](https://gitlab.com/agustin.moyano/sfc#FlowOne)__ <img src="static/images/flow_one.svg" width="20" style="vertical-align: middle; width: 20px"/>: Check all piped streams, and flow data to the first that matches criteria.
  ![FlowOne](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/FlowOne_loop.gif)

* __[FlowWait](https://gitlab.com/agustin.moyano/sfc#FlowWait)__ <img src="static/images/flow_wait.svg" width="20" style="vertical-align: middle; width: 20px"/>: Wait untill every source ended to emit end event.
  ![FlowWait](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/FlowWait_loop.gif)

* __[Rule](https://gitlab.com/agustin.moyano/sfc#Rule)__ <img src="static/images/rule.svg" width="20" style="vertical-align: middle; width: 20px"/>: A rule represents a piece of business logic. Streams can be chained by an arbitrary event name, or by resolve/reject functions.
  ![Rule](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/Rule_loop.gif)
  
* __[Goal](https://gitlab.com/agustin.moyano/sfc#Goal)__ <img src="static/images/goal.svg" width="20" style="vertical-align: middle; width: 20px"/>: Goal wrapps many streams within, hidding complex business logic. Represents a goal to achieve.
  ![Goal](https://gitlab.com//agustin.moyano/sfc/-/raw/master/docs/images/Goal_loop.gif)

### Custom streams

You are able to define custom stream types to reuse

![Custom Type](doc/images/custom_types.gif)
### YAML style multiple documents

You can have a multiple document yaml, or a json file that starts with an array.

![Multiple Docs](doc/images/multi_doc.gif)

### Goal navigation

Double click on Goal streams to navigate to it's children

![Goal Navigation](doc/images/goal_navigation.gif)

### Reorder messy diagrams

![Reorder](doc/images/reorder.gif)

<!-- Describe specific features of your extension including screenshots of your extension in action. Image paths are relative to this README file.

For example if there is an image subfolder under your extension project workspace:

\!\[feature X\]\(images/feature-x.png\)

> Tip: Many popular extensions utilize animations. This is an excellent way to show off your extension! We recommend short, focused animations that are easy to follow. -->

## Requirements

In order for your stream flows to run, you must have installed sfc in your project.
### Version compatibility

| Editor version | Max SFC version |
| --- | --- |
| 1.0.1 | 1.3.x |
| 1.2.0 | 1.4.x |


### File pattern

Stream Flow Editor recognizes files with the following pattern

* Files with extension yaml, yml or json that lives in a directory named 'sfc'
* Files which name ends with '.sfc.yaml', '.sfc.yml' or '.sfc.json'
* YAML files which extension is '.sfc' (Not recomended. You won't have syntax highlighting or text completion) 
<!-- ## Requirements

If you have any requirements or dependencies, add a section describing those and how to install and configure them.

## Extension Settings

Include if your extension adds any VS Code settings through the `contributes.configuration` extension point.

For example:

This extension contributes the following settings:

* `myExtension.enable`: enable/disable this extension
* `myExtension.thing`: set to `blah` to do something -->

<!-- ## Known Issues

Calling out known issues can help limit users opening duplicate issues against your extension.

## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release of ...

### 1.0.1

Fixed issue #.

### 1.1.0

Added features X, Y, and Z.

-----------------------------------------------------------------------------------------------------------

## Working with Markdown

**Note:** You can author your README using Visual Studio Code.  Here are some useful editor keyboard shortcuts:

* Split the editor (`Cmd+\` on macOS or `Ctrl+\` on Windows and Linux)
* Toggle preview (`Shift+CMD+V` on macOS or `Shift+Ctrl+V` on Windows and Linux)
* Press `Ctrl+Space` (Windows, Linux) or `Cmd+Space` (macOS) to see a list of Markdown snippets

### For more information

* [Visual Studio Code's Markdown Support](http://code.visualstudio.com/docs/languages/markdown)
* [Markdown Syntax Reference](https://help.github.com/articles/markdown-basics/)

**Enjoy!** -->
