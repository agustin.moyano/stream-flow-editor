const {sfc} = require('stream-flow-control');

sfc.pause().parseFiles('sfc/multi_document.yml').resume();